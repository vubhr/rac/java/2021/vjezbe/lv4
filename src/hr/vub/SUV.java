package hr.vub;

public class SUV extends Car{

    private int passengerCapacity;
    private double weightInKilos;
    private int passengersInCars;
    private double passengerWeight = 95.0;

    public SUV(double literPer100Km, double topSpeedInKmh, double fuelTankCapacity, double weightInKilos, int passengerCapacity ) {
        super(literPer100Km, topSpeedInKmh, fuelTankCapacity, weightInKilos);
        System.out.println("Setting variables in SUV object!");
        this.passengerCapacity = passengerCapacity;
        passengersInCars = 1;
        this.weightInKilos = 1900;
    }

    public SUV(){
        this(8.6, 200, 60, 1000, 5);
    }

    public void setPassengersInCars(int passengersInCars) {
        if(passengersInCars <= passengerCapacity) {
            this.passengersInCars = passengersInCars;
        } else {
            System.out.println("You can fit so much people in car!");
        }
    }

    private double getWeightFactor(){
       return weightInKilos/ (super.weightInKilos + (passengersInCars * passengerWeight ));
    }

    @Override
    public double rangeInKm() {
        return  100 * fuelTankCapacity * getWeightFactor()/litersPer100Km;
    }

    @Override
    public double realisticTopSpeed() { return  topSpeedInKmh * getWeightFactor(); }
}
