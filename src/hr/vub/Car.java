package hr.vub;

public abstract class Car {

    protected double litersPer100Km;
    protected double topSpeedInKmh;
    protected double fuelTankCapacity;
    protected double weightInKilos;


    public Car(double literPer100Km, double topSpeedInKmh, double fuelTankCapacity, double weightInKilos) {
        System.out.println("Setting variables in abstract car class!");
        this.litersPer100Km = literPer100Km;
        this.topSpeedInKmh = topSpeedInKmh;
        this.fuelTankCapacity = fuelTankCapacity;
        this.weightInKilos = weightInKilos;

    }

    public Car() {
        this(6.5, 170, 50, 875);
    }

    abstract public double rangeInKm();
    abstract public double realisticTopSpeed();

}
