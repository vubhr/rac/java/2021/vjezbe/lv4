package hr.vub;

public class Main {

    public static void main(String[] args) {

        SUV suv = new SUV(8.6, 200, 60, 1000, 5);
        System.out.println("Perfect top speed: " + suv.topSpeedInKmh);
        System.out.println("Range in kilometers: " + suv.rangeInKm());
        System.out.println("Realistic top speed: " + suv.realisticTopSpeed());

        SportsCar sc = new SportsCar(17.3, 340, 40, 1250, 35);
        System.out.println("Perfect top speed: "  + sc.topSpeedInKmh);
        System.out.println("Range in kilometers: " + sc.rangeInKm());
        System.out.println("Realistic top speed: " + sc.realisticTopSpeed());

    }
}
