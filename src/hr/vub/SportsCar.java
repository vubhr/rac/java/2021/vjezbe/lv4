package hr.vub;

public class SportsCar extends Car {
    double tireWidthCm;

    public SportsCar(double literPer100Km, double topSpeedInKmh, double fuelTankCapacity, double weightInKilos, double tireWidth) {
        super(literPer100Km, topSpeedInKmh, fuelTankCapacity, weightInKilos);
        System.out.println("Setting variables in SportsCar!");
        this.tireWidthCm = tireWidth;

    }
    public SportsCar(){
        this(17.3, 340, 40, 1250, 35 );
    }

    private  double getTireCoefficent(){
        return  tireWidthCm /30;
    }

    @Override
    public double rangeInKm() {
        return 100 * fuelTankCapacity * (2-getTireCoefficent())/fuelTankCapacity;
    }

    @Override
    public double realisticTopSpeed() {
        return topSpeedInKmh * getTireCoefficent();
    }
}
